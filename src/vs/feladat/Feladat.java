package vs.feladat;

import java.util.Scanner;

public class Feladat {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println(" Írj be egy szöveget : ");
		String text = sc.nextLine();
		System.out.println(" Az eredeti szöveg :" + text);
		
		text = removeWhitespace(text);

		text = legyenKisbetus(text);

		System.out.println("Palindróm :" + isPalindrom(text));
	}

	private static String legyenKisbetus(String text) {
		return text.toLowerCase();
	}

	private static String removeWhitespace(String text) {
		return text.replace(" ", "");
	}

	private static boolean isPalindrom(char[] karakterek) {
		for (int i = 0; i < (karakterek.length) / 2; i++) {

			if (karakterek[i] != karakterek[(((karakterek.length) - i) - 1)]) {
				return false;
			}
		}
		return true;
	}

	static boolean isPalindrom(String text) {
		return isPalindrom(text.toCharArray());
	}
}
